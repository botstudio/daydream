module.exports = {
    name: "dns",
    description: "Resolve an address",
    category: "util",
    active: true,
    execute() {
        const { client, message, args, db, embed } = require("../app")
        const fetch = require("node-fetch")

        if (!args[0]) return message.channel.send("<:IconProvide:553870022125027329> Unknown Resolve Target")
        fetch("https://dns.google.com/resolve?type=PTR&name=" + args.join(" ")).then(res => {
            res.json().then((body) => {
                if (!body.Status === 0) return message.channel.send("Your query is unresolvable.\nTry with a different one?")
                const embedRes = new embed()
                embedRes
                    .setColor(0x212121)
                    .setTitle(
                        body.Question[0].name.split(".")
                            .slice(0, body.Question[0].name.split(".").length - 1)
                            .join(".") + " (type " + body.Question[0].type
                        + ") resolves to:")
                    .addField("Type", body.Authority[0].type || "<unknown>")
                    .addField("Time to live (TTL)", body.Authority[0].TTL || "<unknown>")
                    .addField("Nameserver", body.Authority[0].data.split(". ")[0] || "<unknown>")
                    .addField("DNS Hostmaster", body.Authority[0].data.split(". ")[1] || "<unknown>")
                    .addField("Mystery Text", body.Authority[0].data.split(". ")[2] || "<unknown>")
                    .setFooter(body.Comment)
                message.channel.send(embedRes)
            })
        })
    },
}