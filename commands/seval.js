const stopwatch = require("../lib/stopwatch")

module.exports = {
    name: "seval",
    admin: true,
    description: 'Evaluates JS Code, but silently',
    category: "meta",
    async execute() {
        const { client, message, args, db } = require("../app")

        if (!client.cfg.admins.includes(message.author.id)) return // Double check, even though admin should cover this

        const clean = (text) => {
            return text.replace(client.token, '「ｒｅｄａｃｔｅｄ」').replace(/`/g, `\`${String.fromCharCode(8203)}`).replace(/@/g, `@${String.fromCharCode(8203)}`)
        }

        const evaluate = async (code) => {
            code = code.replace(/[“”]/g, '"').replace(/[‘’]/g, "'")

            try {
                eval(code)
            } catch (e) {
                const m = await message.channel.send("No...")
                const filter = (reaction, user) => {
                    return ['❌'].includes(reaction.emoji.name) && user.id === message.author.id
                }
                m.react("❌")
                m.awaitReactions(filter, { max: 1, time: 60000, errors: ['time'] })
                    .then(collected => {
                        const reaction = collected.first()

                        if (reaction.emoji.name === '❌') {
                            m.delete()
                        }
                    })
                    .catch(() => {
                        m.delete()
                    })
            }
        }

        void evaluate(args.join(" "))
    },
}