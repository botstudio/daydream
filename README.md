# Daydream  
  
A Discord bot made to replace nothing, and to create something.  
  
## Invite me  
  
[Admin Invite](https://discordapp.com/api/oauth2/authorize?client_id=505305416276901898&permissions=8&scope=bot)  
  
## Get Redis

See the steps outlined at [redis.io](https://redis.io).  
For Windows: [MicrosoftArchive/redis @ GitHub](https://github.com/microsoftarchive/redis).

## Config example  
  
```json
{
    "token": "Discord Bot Token",
    "defPrefix": "Default Prefix",
    "admins": [
        "User IDs"
    ],
    "icons": false,
    "ksoft": "KSoft.si API Key",
    "contact": "Your E-mail",
    "pixabay": "Pixabay API Key",
    "bugreportChannel": "Channel ID for Bugs",
    "secureChannelForVulnerabilitiesDenyEveryoneReadMessagesPermPls": "Channel ID for **critical** bugs",
    "fixedBugsChannel": "Channel ID for fixed Bugs",
    "version": "1.0.0",
    "gfonts": "Google Fonts API Key",
    "redis": {
        "host": "localhost",
        "port": "<dd-ignorethis>",
        "password": "<dd-ignorethis>"
    }
}
```  
The redis part is relevant if you're hosting redis in the cloud, i.e. on RedisLabs (like the official instance does) but I assume you know how to fill out a JSON if you're doing this.  

Before running Daydream the first time, execute `npm install` in order to install the dependencies.  
Then, `redis-server` to start a redis server. (Be aware you will have to have Redis running the whole time while Daydream is.) 
After this, grab Spartan from Google Fonts using [this link](https://fonts.google.com/?selection.family=Spartan:wght@400;700;800&sidebar.open) (use the **Download family** button in the sidebar) and put them in the `imageResources` directory, like this:  
```
Spartan Bold - spartan-bold.ttf
Spartan Extra-Bold - spartan-ebold.ttf
Spartan Regular - spartan.ttf
```  
Then run with `node app.js config_filename_in_config_directory`.  
 
# Alternatives  
  
If you wish to not use the official instance (nor selfhost your own), there are some alternatives to this bot.  
  
| Name       | Quote                                       | Author             | Language | Database        | License | Repo                                                                     | Public Instance                                                                                                                       |
|------------|---------------------------------------------|--------------------|----------|-----------------|---------|--------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------|
| Nightdream | A Clone of the Discord Bot Daydream in Java | JDiscordBots | Java     | Properties, all SQL Databases(with a JDBC driver), Redis | BSL-1.0 | GitHub:  [JDiscordBots/NightDream](https://github.com/JDiscordBots/Nightdream) | [596643235523330070](https://discordapp.com/api/oauth2/authorize?client_id=596643235523330070&permissions=8&scope=bot) (Admin Invite) |
| Raindream  | Raindream is a bot loosely based on my bot Daydream done in C# | Infi (that's me) | C#    | Undecided, Likely Redis | ISC | Infi's Git:  [infi/raindream](https://git.geist.ga/infi/Raindream) | I have Daydream already |  
  
I'd personally recommend Nightdream for quick self-hosting because it is more complete, but Raindream is officially supported by the creator of Daydream.

<sup>If you think your alternative deserves to be on this list, open an issue</sup>