module.exports = {
    name: "info",
    description: "Displays bot information",
    active: true,
    category: "meta",
    execute() {
        const { client, message, args, db } = require("../app")

        if (message.guild.me.permissions.has("ATTACH_FILES")) {
            const processor = require("../lib/imageProcessing")
            message.reply(processor.info(client))
        } else {
            if (message.guild.me.permissions.has("USE_EXTERNAL_EMOJIS")) {
                // 583 chars in a line.
                message.channel.send(`In ${client.guilds.size} guilds, serving ${client.users.size} users.\nThis instance is owned by ${client.users.get(client.cfg.admins[0]).tag}.\ndiscord.js v${(require("../node_modules/discord.js/package.json")).version}\nLogo Font: Avenir Next LT Pro / © Linotype${/*applies to official instances only*/""}\n© BotStudio ${new Date().getFullYear()}, Release ${client.cfg.version}.`)
            } else {
                message.channel.send(`**Unable to use external emojis, likely to break on other commands!**\n\nIn ${client.guilds.size} guilds, serving ${client.users.size} users.\nThis instance is owned by ${client.users.get(client.cfg.admins[0]).tag}.\ndiscord.js v${(require("../node_modules/discord.js/package.json")).version}\nLogo Font: Avenir Next LT Pro / © Linotype${/*applies to official instances only*/""}\n© BotStudio ${new Date().getFullYear()}, Release ${client.cfg.version}.`)
            }
        }
    }
}