const { exec } = require("child_process")

module.exports = {
    name: "shell",
    description: "Run a shell command",
    category: "util",
    active: true,
    admin: true,
    execute() {
        const { client, message, args, db, embed } = require("../app")

        message.channel.send(`Executing \`${(args.join(" ") || "<nothing>")}\` in a \`${process.platform}\` environment...`).then((m) => {
            const embedShell = new embed()
                .setColor(0x212121)
                .addField("Input", `\`\`\`${(args.join(" ") || "<nothing>")}\`\`\``, true)

            const childInfo = {
                exitCode: -1
            }
            const timeouts = {}

            const child = exec((args.join(" ") || "echo \\<provide an input\\>"), (err, stdout, stderr) => {
                clearTimeout(messageTimeout)
                clearTimeout(killTimeout)

                if (err || stderr || stdout) embedShell.addBlankField()

                if (err || stderr) embedShell.addField("Error (`stderr`)", "```" + String(err || stderr).substr(0, 512) + "```", true)
                if (stdout) embedShell.addField("Output (`stdout`)", "```" + stdout.substr(0, 512) + "```", true)

                const duration = Date.now() - m.createdAt

                embedShell.setFooter("Took me " + ((duration < 0) ? duration : "around 0") + "ms.")
                m.edit(`Executed \`${(args.join(" ") || "<nothing>")}\` in a \`${process.platform}\` environment. Exit code ${childInfo.exitCode}`, { embed: embedShell })
            })

            child.on("exit", (code) => {
                childInfo.exitCode = code
            })

            const messageTimeout = setTimeout(() => {
                message.channel.send("Excuse the wait, the command is still executing! The process will be killed in 10 more seconds.")
            }, 10000)
            const killTimeout = setTimeout(() => {
                child.kill() // o.o
                message.channel.send("Process was killed after being unresponsive for 20 seconds.")
            }, 20000)
        })

    },
}