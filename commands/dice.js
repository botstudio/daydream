module.exports = {
    name: "dice",
    description: "Rolls a random number from one",
    active: true,
    category: "util",
    async execute() {
        const { client, message, args, db } = require("../app")
        const embed = (require("discord.js")).RichEmbed

        if (isNaN(args[0])) return message.channel.send("<:IconProvide:553870022125027329> Not enough arguments!")
        const embedDice = new embed()
            .setColor(0x212121)
            .setTitle("Rolling the dice...")
            .setDescription("From 1 to " + args[0])
        const m = await message.channel.send(embedDice)
        const updateEmbed = () => {
            embedDice
                .setTitle("Done!")
                .setDescription("It landed on a " + Math.floor(Math.random() * parseInt(args[0]) + 1))
            m.edit(embedDice)
        }
        setTimeout(updateEmbed, Math.floor(Math.random() * 3000) + 1000)
    },
}