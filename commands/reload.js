module.exports = {
    name: "reload",
    description: "Reload parts of the bot",
    active: true,
    admin: true,
    category: "config",
    async execute() {
        const { client, message, args, db } = require("../app")

        const options = ["login"]
        if (!options.includes(args[0])) return message.channel.send(`args[0] must be: \`[${options.join(", ")}]\``)
        if (args[0] === "login") {
            const m = await message.channel.send("Destroying client...")
            client.destroy().then(() => {
                client.login(client.cfg.token)
                m.edit("Reload successful")
            })
        }
    },
}