module.exports = {
    name: "photo",
    active: true,
    description: "Gets a photo from Pixabay",
    category: "image",
    execute: () => {
        const { client, message, args, db, embed } = require("../app")

        message.channel.startTyping()
        if(!args[0]) return message.channel.send("<:IconProvide:553870022125027329> Search Query, please.")
        const fetch = require("node-fetch")
        fetch(encodeURI("https://pixabay.com/api/?image_type=photo&key=" + client.cfg.pixabay + "&q=" + args.join(" ")), {
            headers: {
                "User-Agent": "Daydream Bot <" + client.cfg.contact + "> | node-fetch " + (require("../node_modules/node-fetch/package.json").version)
            }
        })
        .then(response => {
            if (!response.ok) return message.channel.send("<:IconX:553868311960748044> Something went badly wrong - the server did not respond! Try again **in a few minutes**.")
            response.json().then(body => {
                const embedPixabay = new embed()
                    .setColor(0x212121)
                
                if (!body.hits[0]) {
                    embedPixabay
                        .setTitle("<:IconProvide:553870022125027329> Nothing found")
                        .setDescription("Try something different.")
                } else {
                    embedPixabay
                        .setFooter("Results from Pixabay [https://pixabay.com]")
                        .setTitle("Result")
                        .setImage(body.hits[0].largeImageURL)
                }

                message.channel.send(embedPixabay)
            })
        })
        message.channel.stopTyping()
    },
}