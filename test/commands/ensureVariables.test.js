/* eslint-disable no-undef */

const assert = require("assert")
const commands = new (require("discord.js")).Collection()
const fs = require("fs")

const cmdFiles = fs
    .readdirSync((require("path").join(__dirname, "..", "..", "commands")))
    .filter(a => a.endsWith(".js"))
cmdFiles.forEach(f => {
    commands.set(f.split(".")[0], require("../../commands/" + f))
})

describe("Administrator requirement", () => {
    it("eval requires admin", () => {
        assert.strictEqual(commands.get("eval").admin, true)
    })
    it("seval requires admin", () => {
        assert.strictEqual(commands.get("seval").admin, true)
    })
    it("shell requires admin", () => {
        assert.strictEqual(commands.get("shell").admin, true)
    })
    it("activity requires admin", () => {
        assert.strictEqual(commands.get("activity").admin, true)
    })
    it("banfrombugs requires admin", () => {
        assert.strictEqual(commands.get("banfrombugs").admin, true)
    })
    it("fixed requires admin", () => {
        assert.strictEqual(commands.get("fixed").admin, true)
    })
    it("reload requires admin", () => {
        assert.strictEqual(commands.get("reload").admin, true)
    })
})

describe("Categories", () => {
    const categories = [
        "util",
        "fun",
        "config",
        "meta",
        "image"
    ]
    it("Every command has a category", () => {
        assert.strictEqual(commands.array().filter(x => Boolean(x.category)).length, commands.size)
    })
    it("All categories are valid", () => {
        assert.strictEqual(commands.array().filter(x => categories.includes(x.category)).length, commands.size)
    })
})