module.exports = {
    active: true,
    name: 'choose',
    description: 'Chooses an option from a list',
    category: "fun",
    async execute() {
        const { client, message, args, db, embed } = require("../app")

        if (!args[1]) {
            return message.channel.send("<:IconX:553868311960748044> Please give me some options!")
        }
        const chosen = args[Math.floor(Math.random() * args.length)]
        const em = new embed()
            .setColor(0x212121)
            .setTitle("I've chosen!")
            .setDescription(chosen)
        message.channel.send(em)
    },
}