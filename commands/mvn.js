module.exports = {
    name: "mvn",
    description: "Like npm but worse (and for maven artifacts)",
    active: true,
    category: "util",
    execute() {
        const { client, message, args, db, embed } = require("../app")
        const fetch = require("node-fetch")

        const query = args.join(" ")
        if (!query) return message.channel.send("<:IconProvide:553870022125027329> I need a query")
        fetch("http://search.maven.org/solrsearch/select?q=" + query + "&wt=json")
            .then(response => response.json().then(body => {
                const pkg = body.response.docs[0]
                const embedMaven = new embed()
                    .setColor(0xdc6328)
                if (!pkg) {
                    embedMaven
                        .setTitle("<:IconProvide:553870022125027329> Nothing found")
                        .setDescription("Try something different.")
                } else {
                    embedMaven
                        .setTitle("Result")
                        .addField("Group ID", `\`${pkg.g}\``, true)
                        .addField("Artifact ID", `${pkg.a}`, true)
                        .addField("Current Version", `${pkg.latestVersion}`, true)
                        .addField("Repository", `${pkg.repositoryId}`, true)
                }
                message.channel.send(embedMaven)
            }))
    },
}