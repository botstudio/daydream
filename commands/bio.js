module.exports = {
    name: "bio",
    description: "The first Discord-based discord.bio UI",
    category: "util",
    active: true,
    execute() {
        const { client, message, args, db, embed } = require("../app")
        const fetch = require("node-fetch")

        if (!args[0]) return message.channel.send("<:IconProvide:553870022125027329> Provide at least one argument (the user slug)")

        const base_url = "https://api.discord.bio/v1"
        const discord_cdn_base_url = "https://cdn.discordapp.com/avatars"
        const slug = args[0]

        const oof = () => {
            return message.channel.send("<:IconX:553868311960748044> Something went wrong.\n\nThis command is in beta for now, this means among other things that bugs are not automatically reported.\nPlease use `" + client.prefix + "bugreport` to report the bug manually with the exact input provided.")
        }

        const genderMapping = {
            null: "Unspecified",
            1: "Male",
            2: "Female",
            3: "Other"
        }

        const embedDscBio = new embed()
            .setColor(0x212121)
        fetch(`${base_url}/userDetails/${slug}`, {
            headers: {
                "User-Agent": `Daydream ${client.cfg.version} | ${client.cfg.contact}`
            }
        }).then(
            resUD => resUD.json().then(
                bodyUD => {
                    if (!bodyUD.success) oof()
                    else {
                        const userInfo = bodyUD.payload
                        const avatarKey = userInfo.discord.avatar
                        embedDscBio
                            .setTitle(`${userInfo.discord.username}#${userInfo.discord.discriminator}`)
                            .setThumbnail(
                                `${discord_cdn_base_url}/${userInfo.discord.id}/${avatarKey}.${avatarKey.startsWith("a_") ? ".gif" : ".png"}`)
                            .setDescription(userInfo.settings.status)
                            .addFields([
                                ["About", userInfo.settings.description, true],
                                ["Upvotes", userInfo.settings.upvotes, true],
                                ["Location", userInfo.settings.location, true],
                                ["Birthday", userInfo.settings.birthday, true],
                                ["E-mail", userInfo.settings.email, true],
                                ["Occupation", userInfo.settings.occupation, true],
                                ["Verified", userInfo.settings.verified ? "Yes" : "No", true],
                                ["ID", userInfo.discord.id, true],
                                ["Gender", genderMapping[userInfo.settings.gender], true]
                            ])
                            .setColor(0x212121)
                        message.channel.send(embedDscBio)
                    }
                }
            )
        )
    },
}