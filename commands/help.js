module.exports = {
    name: "help",
    description: '¯\\\\\\_(ツ)_/¯',
    active: true,
    category: "meta",
    execute() {
        const { client, message, args, embed, cats, commands } = require("../app")

        const dicesCoefficient = require("string-similarity")

        const capitalizeFirst = (input) => {
            return input.charAt(0).toUpperCase() + input.slice(1)
        }

        if (args[0]) {
            if (client.commands.get(args[0])) {
                const command = client.commands.get(args[0])
                const embedHelpCommand = new embed()
                    .setColor(0x212121)
                    .setTitle("Help with " + args[0])
                    .addField("Category", command.category, true)
                    .addField("Description", command.description, false)
                message.channel.send(embedHelpCommand)
            } else {
                const matches = dicesCoefficient.findBestMatch(args[0], commands)
                const embedHelpCommandUnknown = new embed()
                    .setColor(0x212121)
                    .setTitle("Unknown Command")
                    .setDescription("But I have `" + matches.bestMatch.target + "`.")
                message.channel.send(embedHelpCommandUnknown)
            }
        } else {
            const embedHelp = new embed()
                .setColor(0x212121)
                .setTitle("Daydream Commands")
                .setFooter(`Prefix in ${message.guild.name}: ${client.prefix} | ${client.prefix}help <command> for command help`)
            cats.forEach(cat => {
                let details = ""
                client.commands
                    .filter(cmd => cmd.category === cat && !cmd.admin && cmd.active !== false)
                    .forEach(command => {
                        details += `\n\`${command.name}\` - ${command.description}`
                    })
                embedHelp.addField(capitalizeFirst(cat), details, false)
            })
            message.channel.send(embedHelp)
        }
    },
}