module.exports = {
    active: true,
    name: 'activity',
    description: 'Changes the bot\'s activity',
    admin: true,
    category: "config",
    execute() {
        const { client, message, args, db } = require("../app")

        db.set("status", args.join(" "), (err, ok)=>{
            if (ok) {
                db.get("status", (err,val) => {
                    client.user.setPresence({
                        "game": {
                            "name": client.cfg.defPrefix + "help | " + val
                        }
                    }).then(message.channel.send("Done: " + client.cfg.defPrefix + "help | " + val))
                })
            }
        })
    },
}