module.exports = {
    name: "neko",
    active: true,
    description: "Sends a neko image",
    category: "image",
    async execute() {
        const { client, message, args, db, embed } = require("../app")

        message.channel.startTyping()
        const fetch = require("node-fetch")
        fetch("https://api.ksoft.si/images/random-image?tag=neko&nsfw=true", { // It won't ever be NSFW, according to API executives,   |
            headers: {                                                         // but it still needs to have the query parameter.       ↲ 
                "Authorization": "Bearer " + client.cfg.ksoft,
                "User-Agent": "Daydream Bot <" + client.cfg.contact + "> | node-fetch " + (require("../node_modules/node-fetch/package.json").version)
            }
        }).then(response => {
            if (!response.ok) return message.channel.send("Errored while trying to connect to server.")
            response.json().then(body => {
                const embedNeko = new embed()
                    .setColor(0x212121)
                    .setTitle("Here's a neko~")
                    .setImage(body.url)
                    .setFooter("Served by an external API (KSoft.Si) - report with " + client.prefix + "bugreport [url]")
                message.channel.send(embedNeko)
            })
        })
        message.channel.stopTyping()
    },
}