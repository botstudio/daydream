module.exports = {
    name: "avatar",
    description: "Shows your (or someone else's) Avatar",
    active: true,
    category: "util",
    execute() {
        const { client, message, args, db, embed, logg } = require("../app")

        const determineTarget = () => {
            if (message.mentions.members.first()) {
                return message.mentions.members.first().user
            } else {
                return message.author
            }
        }

        const url = determineTarget().avatarURL

        const embedAvatar = new embed()
            .setColor(0x212121)
        if (url) {
            embedAvatar.setImage(determineTarget().avatarURL)
        } else {
            embedAvatar.setDescription("<nothing>")
        }
        message.channel.send(embedAvatar)
    },
}