const Long = require("long")

module.exports = {
    active: true,
    name: 'snow',
    description: 'Discord ID deconstructor/generator',
    category: "fun",
    execute() {
        const { client, message, args, db, embed } = require("../app")

        const yes_ishCheck = (val) => {
            if (val) return true; else return false
        }

        const util = (require("discord.js")).SnowflakeUtil
        if (args[0]) {
            const asNumber = Number(args[0])
            if (Math.sign(asNumber) === -1 || isNaN(asNumber)) {
                return message.channel.send(" <:IconThis:553869005820002324> Must be a positive long.")
            }
            const dec = util.deconstruct(args[0])
            const embedSnow = new embed()
                .setTitle(args[0])
                .setColor(0x212121)
            if (Number(args[0]) > Long.MAX_UNSIGNED_VALUE) {
                embedSnow.addField("Warning", `This number is above Long.MAX_UNSIGNED_VALUE (${Long.MAX_UNSIGNED_VALUE}).\nYou're on your own.`)
            }
            embedSnow
                .addField("Binary", (dec.binary === 0 ? "lolno" : dec.binary))
                .addField("Date", dec.date)
                .addField("Increment", dec.increment)
                .addField("Worker, Process ID", args[0] + " has worker ID " + dec.workerID + " with process ID " + dec.processID + ".")
            message.channel.send(embedSnow)
        } else {
            message.channel.send(`\`${util.generate()}\` made \`${new Date()}\``)
        }
    },
}