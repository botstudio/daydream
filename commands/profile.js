module.exports = {
    name: "profile",
    description: `Shows & manages your profile, \`profile help\` for more`,
    active: true,
    category: "fun",
    execute: async () => {
        const { client, message, args, db, embed } = require("../app")
        const util = require("util")
        
        const getAsync = util.promisify(db.get).bind(db)

        if (message.mentions.users.array().length !== 0) {
            const u = message.mentions.users.first()
            const embedProfileOther = new embed()
            embedProfileOther.setColor(await getAsync("p_color_" + u.id) || 0x212121)
                .setTitle(await getAsync("p_name_" + u.id) || u.tag)
                .setDescription(await getAsync("p_desc_" + u.id) || "A Ghost... yet")
            if (client.cfg.admins.includes(u.id)) embedProfileOther.addField("<:IconInfo:553868326581829643> Bot Admin!", "This is a bot admin.")
            return message.channel.send(embedProfileOther)
        }
        if (args[0] === "description" || args[0] === "desc") {
            if (!args[1]) return message.channel.send("<:IconProvide:553870022125027329> I need more than 1 argument.")
            const embedDescSuccess = new embed()
            const desc = args.slice(1).join(" ")
            embedDescSuccess.setDescription(desc).setTitle("Your description is now").setColor(0x212121)
            db.set("p_desc_" + message.author.id, desc)
            message.channel.send(embedDescSuccess)
        } else if (args[0] === "color") {
            const embedColSuccess = new embed({ title: "Set color!" })
            if (!args[1] || args[1].length !== 7) return message.channel.send("Format <:IconThis:553869005820002324> `" + client.prefix + "profile color #123456`")
            db.set("p_color_" + message.author.id, args[1])
            embedColSuccess.setColor(await getAsync("p_color_" + message.author.id))
            message.channel.send(embedColSuccess)
        } else if (args[0] === "help") {
            const embedPhelp = new embed()
                .setTitle("Profile Help")
                .setColor(0x212121)
                .addField("color", "Sets a profile color in #123456 format")
                .addField("description/desc", "Sets a profile description")
                .addField("name", "Sets your name")
            message.channel.send(embedPhelp)
        } else if (args[0] === "name") {
            if (!args[1]) return message.channel.send("Format <:IconThis:553869005820002324> `" + client.prefix + "profile link Name http(s)://link`")
            const embedNameSuccess = new embed()
                .setColor(0x212121)
                .setTitle("Set name!")
            db.set("p_name_" + message.author.id, args.slice(1).join(" "))
            embedNameSuccess.setDescription("It is now " + await getAsync("p_name_" + message.author.id) + ".")
            message.channel.send(embedNameSuccess)
        } else {
            const embedProfile = new embed()
            embedProfile.setColor(await getAsync("p_color_" + message.author.id) || 0x212121)
                .setTitle(await getAsync("p_name_" + message.author.id) || message.author.tag)
                .setDescription(await getAsync("p_desc_" + message.author.id) || "A Ghost... yet")

            if (client.cfg.admins.includes(message.author.id)) embedProfile.addField("<:IconInfo:553868326581829643> Bot Admin!", "This is a bot admin.")

            message.channel.send(embedProfile)
        }
    }
}