module.exports = {
    name: "fixed",
    admin: true,
    description: "Reports a bug as fixed",
    category: "meta",
    execute() {
        const { client, message, args, db, embed } = require("../app")

        const info = {}
        const msgSplit = args.join(" ").split("|")
        info.id = msgSplit[0]
        info.original = msgSplit[1]
        info.additional = msgSplit[2]
        if (!info.id || !info.original) return message.channel.send("<:IconProvide:553870022125027329> I need more info. (ID|Original Bug)")
        const embedFixed = new embed()
            .setColor(0x212121)
            .setFooter("Reported as fixed by " + message.author.username)
            .setTitle("Fixed bug " + info.id)
            .addField("Original Bug", info.original)
        if (info.additional) {
            embedFixed.addField("Additional comment", info.additional)
        }
        client.channels.get(client.cfg.fixedBugsChannel).send(embedFixed)
    },
}