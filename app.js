const logger = require("daydream-logging")
const Discord = require("discord.js")
const client = new Discord.Client({
    disableEveryone: true,
})
const fs = require("fs")
const didYouMean = require("string-similarity")
const util = require("util")
const path = require("path")

const configs = fs.readdirSync(path.join(__dirname, "config"))

if (!process.argv[2]) {
    logger(
        "fatal",
        "Profile argument missing! (e.g. node app.js test)",
        "Profiles"
    )
    process.exit(1)
} else if (!configs.includes(process.argv[2] + ".json")) {
    logger(
        "fatal",
        "Profile does not exist in config/ folder. Is it valid JSON with an extension?",
        "Profiles"
    )
    process.exit(1)
}
logger("info", `Loading with profile ${process.argv[2]}`, "Profiles")
const cfg = require("./config/" + process.argv[2] + ".json")

client.cfg = cfg
client.profileName = process.argv[2]

const passToRedis = {}
if (cfg.redis.host !== "localhost") passToRedis.host = cfg.redis.host
if (cfg.redis.port !== "<dd-ignorethis>") passToRedis.port = cfg.redis.port
if (cfg.redis.password !== "<dd-ignorethis>") passToRedis.auth_pass = cfg.redis.password

const db = require("redis").createClient(passToRedis)

db.getAsync = util.promisify(db.get).bind(db)

client.commands = new Discord.Collection()

logger("info", "Loading commands", "Command Handler")
const commandFiles = fs
    .readdirSync("./commands")
    .filter(file => file.endsWith(".js"))

const allowedCategories = [
    "util",
    "fun",
    "config",
    "meta",
    "image"
]

const commandList = []
for (const file of commandFiles) {
    const command = require(`./commands/${file}`)
    if (!command.category || !allowedCategories.includes(command.category)) throw new TypeError("Unallowed category for command " + command.name)
    client.commands.set(command.name, command)
    commandList.push(file.split(".")[0])
    process.stdout.write(file.split(".")[0] + "; ")
}
process.stdout.write("\n")
logger("done", "Commands loaded", "Command Handler")

const handleCommandNotFound = (input, prefix) => {
    const similarCommand = didYouMean.findBestMatch(input, commandList).bestMatch
        .target
    const embedNotFound = new Discord.RichEmbed()
        .setTitle(
            "<:IconProvide:553870022125027329> It seems like this command does not exist."
        )
        .setDescription("Did you mean `" + prefix + similarCommand + "`?")
        .setColor(0x212121)
    return embedNotFound
}

const UX_bytes = (bytes, decimals = 2) => {
    if (bytes === 0) return "0 Bytes"

    const k = 1024
    const dm = decimals < 0 ? 0 : decimals
    const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"]

    const i = Math.floor(Math.log(bytes) / Math.log(k))

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i]
}

client.on("message", message => {
    if (message.author.bot || !message.guild) return

    db.get("prefix_" + message.guild.id, (err, val) => {
        const prefix = val || client.cfg.defPrefix

        client.prefix = prefix

        if (
            message.content.startsWith("<@" + client.user.id) &&
            message.mentions.users.size === 1 &&
            message.content.endsWith(">")
        ) {
            return message.channel.send("My prefix here: `" + prefix + "`")
        }

        if (
            message.content.startsWith("<@" + client.user.id) &&
            message.mentions.users.size === 1 &&
            message.content.toLocaleLowerCase().endsWith("> i messed up") &&
            client.cfg.admins.includes(message.author.id)
        ) {
            message.channel.send(
                "It's fine :smiley:\nI reset the prefix on this guild."
            )
            return db.del("prefix_" + message.guild.id)
        }

        const cmdRaw = message.content.split(/ +/)[0].substr(prefix.length)
        const cmd = client.commands.get(cmdRaw)
        let args = message.content.split(/ +/).slice(1)
        if (!message.member.permissions.has("MENTION_EVERYONE")) {
            args = args.map(x => x
                .replace(/@everyone/gim, "@\u200beveryone")
                .replace(/@here/gim, "@\u200bhere"))
        }
        if (message.mentions.roles.first()) {
            message.mentions.roles.array().forEach(r => {
                if (r.mentionable || message.member.permissions.has("ADMINISTRATOR")) return
                args = args.map(x => {
                    x.replace(`<@&${r.id}>`), `@\u200b${r.name}`
                })
            })
        }

        // This performs basic validity checks
        if (!message.content.toLocaleLowerCase().startsWith(prefix)) return
        if (!client.commands.has(cmdRaw))
            return message.channel.send(handleCommandNotFound(cmdRaw, prefix))
        if (!typeof cmd.active === undefined && cmd.active === false)
            return message.channel.send(
                "<:IconInfo:553868326581829643> This command was disabled by the instance owner."
            )
        if (cmd.admin && !client.cfg.admins.includes(message.author.id))
            return message.channel.send(
                "<:IconInfo:553868326581829643> This is an admin command."
            )

        module.exports.message = message
        module.exports.args = args

        // Permission Handler
        const missing = []
        if (cmd.permissions) {
            cmd.permissions.forEach(perm => {
                if (
                    !message.member.permissions.has(perm) &&
                    !cfg.admins.includes(message.author.id)
                )
                    missing.push(perm)
            })
        }
        if (missing.length > 0)
            return message.channel.send(
                "<:IconInfo:553868326581829643> You lack the following permissions: `" +
                missing.join(", ") +
                "`."
            )

        // Simple error handler
        try {
            cmd.execute()
        } catch (error) {
            console.error(error)
            client.users
                .get(cfg.admins[0])
                .send(`Got \`\`\`js\n${error}\`\`\` while executing dd-${cmd.name}`, {
                    split: true,
                })
            message.reply("An unexpected error occurred! We will get to it soon... <:IconX:553868311960748044>")
        }
    })
})

client.on("error", error => {
    client.users.get(cfg.admins[0]).send(error, { code: "js", split: true })
    logger("error", error, "Errors")
})

client.on("ready", () => {
    db.get("status", (err, val) => {
        client.user.setPresence({
            game: {
                name: client.cfg.defPrefix + "help | " + (val || "Daydreaming..."),
                type: "PLAYING",
            },
        })
        logger("info", `Presence: ${client.user.presence.game.name}`, "Presence")
        logger(
            "info",
            "Ready with User ID " +
            client.user.id.black.bgWhite +
            " and Tag " +
            client.user.tag.black.bgWhite,
            "Discord"
        )
    })
})

client.on("messageDelete", message => {
    db.get("log_" + message.guild.id, (err, val) => {
        if (!val) return
        if (!client.channels.get(val)) return
        if (message.author.bot) return
        const embedDeleted = new Discord.RichEmbed()
            .setColor(0x212121)
            .setTitle("Deleted Message")
            .setFooter(
                `${message.author.tag.substr(
                    0,
                    256
                )} in channel #${message.channel.name.substr(0, 256)}`
            )
            .setTimestamp(Date.now())
            .addField("Message", message.content.substr(0, 1024) || "<nothing>")
        if (message.author.avatarURL)
            embedDeleted.setThumbnail(
                message.author.avatarURL.split("?size")[0] + "?size=128"
            )
        if (message.attachments.size >= 1) {
            let attachments = "\n\n"
            for (const attachment of message.attachments) {
                const info = attachment[1] // array by itself for some reason
                attachments += `[${info.filename}](${info.url}) (${UX_bytes(
                    info.filesize
                )})`
            }
            embedDeleted.addField("Attachments", attachments)
        }
        client.channels.get(val).send(embedDeleted)
    })
})

const checkForConfig = (cfg) => {
    if (
        !cfg.bugreportChannel ||
        !cfg.fixedBugsChannel ||
        !cfg.ksoft ||
        !cfg.token ||
        !cfg.defPrefix ||
        !cfg.pixabay
    ) {
        logger("fatal", "Check your config, keys are missing!", "Config")
        process.exit(403)
    }
    if (!cfg.admins) {
        logger("warn", "No admins found in config!", "Config")
    }
    if (!cfg.contact) {
        logger(
            "warn",
            "Contact info missing in config, you might get banned from APIs!",
            "Config"
        )
    }
}

logger("info", "Checking Config", "Config")
checkForConfig(cfg)
logger("info", "Attempting Authentication", "Discord")
client.login(client.cfg.token)
logger("done", "Authenticated", "Discord")

class embed extends Discord.RichEmbed {
    constructor(data) {
        super(data)
    }

    /**
     * Add multiple fields
     * @param {Array[]} fieldShorthand 
     */
    addFields(fieldShorthand) {
        fieldShorthand.forEach(fs => {
            this.addField(fs[0], fs[1], (fs[2] || false))
        })
        return this
    }
}

module.exports.client = client
module.exports.db = db
module.exports.embed = embed
module.exports.logg = logger
module.exports.cats = allowedCategories
module.exports.commands = commandList
