module.exports = {
    name: "inviteme",
    description: "Invites the bot",
    active: true,
    category: "meta",
    execute() {
        const { client, message, args, db, embed } = require("../app")

        const embedInvite = new embed()
            .setColor(0x212121)
            .setTitle("Invites")
            .setDescription(
                `[Add the bot](https://discordapp.com/api/oauth2/authorize?client_id=${client.user.id}&permissions=8&scope=bot)\n[Server invite](https://discordapp.com/invite/DKEqQVx)`
            )
        message.channel.send(embedInvite)
    }
}
