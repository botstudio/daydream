module.exports = {
    name: "hug",
    description: "Hugs someone or yourself :)",
    active: true,
    category: "image",
    execute() {
        const { client, message, args, db, embed } = require("../app")

        const target = message.mentions.users.first() || message.author
        const name = target.nickname || target.username
        message.channel.startTyping()
        const fetch = require("node-fetch") 
        fetch("https://api.ksoft.si/images/random-image?tag=hug", {
            headers: {
                "Authorization": "Bearer " + client.cfg.ksoft,
                "User-Agent": "Daydream Bot <" + client.cfg.contact + "> | node-fetch " + (require("../node_modules/node-fetch/package.json").version)
            }
        }).then(response => {
            response.json().then(body => {
                const embedHug = new embed()
                    .setColor(0x212121)
                    .setImage(body.url)
                    .setTitle(`**${name}** has been hugged by **${(message.member.displayName)}**!`)
                    .setFooter("Served from an external source (KSoft.Si), so please report NSFW with the `bugreport` command.")
                message.channel.send(embedHug)
            })
        })
        message.channel.stopTyping()
    },
}