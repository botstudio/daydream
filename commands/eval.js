const stopwatch = require("../lib/stopwatch")
const Discord = require("discord.js")

module.exports = {
    name: "eval",
    admin: true,
    description: 'Evaluates JS Code',
    category: "meta",
    execute() {
        // The original source code was adapted from Aero, made by ravy and others. Check it out! https://git.farfrom.earth/aero/aero

        const { client, message, args, db, embed } = require("../app")

        if (!client.cfg.admins.includes(message.author.id)) return

        const clean = (text) => {
            return text
                .replace(client.token, '「ｒｅｄａｃｔｅｄ」')
                .replace(/`/g, `\`${String.fromCharCode(8203)}`)
                .replace(/@/g, `@${String.fromCharCode(8203)}`)
        }

        const evaluate = (code) => {
            code = code.replace(/[“”]/g, '"').replace(/[‘’]/g, "'")
            const watch = new stopwatch()
            let success, time, result
            let type

            try {
                result = eval(code)
                time = watch.toString()
                type = typeof result
                success = true
            } catch (e) {
                if (!time) time = watch.toString()
                if (!type) type = typeof e
                result = e
                success = false
            }

            watch.stop()
            if (typeof result !== "string") {
                result = require("util").inspect(result, {
                    depth: 3
                })
            }
            return {
                success,
                time: watch.toString(),
                type,
                result: clean(result)
            }
        }

        const result = evaluate(args.join(" "))

        const embedEvalInfo = new embed()
            .setColor(0x212121)
            .setFooter(`${result.type} | ${result.time}`)

        if (result.result.length > 1990) {
            const attachment = new Discord.Attachment(Buffer.from(result.result), `eval-${Date.now()}.txt`)

            return message.channel.send("As the output was over 2000 characters, it was exported into a text file.", {
                files: [attachment],
                embed: embedEvalInfo
            })
        }
        return message.channel.send(result.result, {
            embed: embedEvalInfo,
            code: "js",
            disableEveryone: true
        })
    }
}