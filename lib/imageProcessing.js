const { createCanvas, registerFont } = require("canvas")
const attachment = (require("discord.js")).Attachment

registerFont("./imageResources/spartan.ttf", { family: "spartan" })
registerFont("./imageResources/spartan-ebold.ttf", { family: "spartan_eb" })

const makeInfoImg = (client) => {
    const canvas = createCanvas(550, 200)
    const ctx = canvas.getContext("2d")

    ctx.fillStyle = "#212121"
    ctx.fillRect(0, 0, canvas.width, canvas.height) // background color
    ctx.font = "23pt spartan_eb"
    ctx.fillStyle = "#e6e6e6"
    ctx.fillText("Bot Info", 20, 40) // heading
    ctx.font = "15pt spartan"
    ctx.fillText(`    In ${client.guilds.size} guilds, serving ${client.users.size} users.
    This instance is owned by ${client.users.get(client.cfg.admins[0]).tag}.
    discord.js v${(require("../node_modules/discord.js/package.json")).version} / node-canvas v${(require("../node_modules/canvas/package.json")).version}
    Font Credits: Spartan by Matt Bailey
    (Logo: Avenir Next LT Pro by Linotype)${/*applies to official instances only*/""}
    © BotStudio ${new Date().getFullYear()}, Release ${client.cfg.version}.`, 30, 70) // main text

    return new attachment(canvas.toBuffer(), "DaydreamInfo.png") // return ready-to-eat attachment, for usage with channel.send()
}

module.exports.info = makeInfoImg
