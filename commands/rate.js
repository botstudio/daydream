module.exports = {
    name: "rate",
    description: 'Rates a User',
    active: true,
    category: "fun",
    execute() {
        const { client, message, args, db, embed } = require("../app")

        const perc = Math.floor((Math.random() * 100) + 1)
        const target = message.mentions.users.first() || message.author
        const embedRate = new embed()
            .setTitle("Rating " + target.username)
            .setDescription(perc + "/100")
            .setColor(0x212121)
        message.channel.send(embedRate)
    },
}