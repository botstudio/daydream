module.exports = {
    name: "bugreport",
    description: "Files a bug report",
    active: true,
    category: "meta",
    async execute() {
        const { client, message, args, db, embed } = require("../app")

        db.smembers("bugs:banned", (error, banned) => {
            if (banned.includes(message.author.id)) return message.channel.send("<:IconInfo:553868326581829643> You were banned from reporting bugs. If you believe this should change, contact the instance owner (see `" + client.prefix + "info`)")
            db.get("bugs:next", (err, val) => {
                if (!val) {
                    db.set("bugs:next", 0)
                    val = 0
                }
                db.incr("bugs:next")
                const bug = args.join(" ")
                if (!bug) return message.channel.send("<:IconProvide:553870022125027329> Please provide a bug to report.")
                const embedBug = new embed()
                    .setTitle("New Bug")
                    .setColor(0x212121)
                    .setDescription(bug)
                    .setFooter(message.author.tag + " (" + message.author.id + ") | Bug ID " + val)
                client.channels.get(client.cfg.bugreportChannel).send(embedBug)

                message.channel.send("Sent with ID " + val)
            })
        })
    }
}
