module.exports = {
    name: "nuget",
    description: "Get Nuget package info",
    category: "util",
    active: true,
    execute() {
        const { client, message, args, db, embed } = require("../app")
        const fetch = require("node-fetch")

        fetch(`https://azuresearch-usnc.nuget.org/query?q=${args[0]}&take=1`).then(res => res.json().then(body => {
            if (body.totalHits <= 0) {
                const embedNuget = new embed()
                    .setColor(0x004980)
                    .setTitle("<:IconProvide:553870022125027329> Nothing found")
                    .setDescription("Try something different.")
                message.channel.send(embedNuget)
            } else {
                const package = body.data[0]

                const embedNuget = new embed()
                    .setColor(0x004980)
                    .setTitle("Result")
                    .addField("Name", package.title, true)
                    .addField("Description", package.description, true)
                    .addField("Namespace", `\`${package.id}\``, true)
                    .addField("Current Version", package.version, true)
                    .addField("Author", package.authors.join(","), true)
                    .addField("Tags", `\`${package.tags.join(",")}\``, true)
                    .addField("Verified", package.verified, true)
                    .addField("Downloads", package.totalDownloads, true)
                    .setThumbnail(package.iconUrl)
                message.channel.send(embedNuget)
            }
        }))
    },
}