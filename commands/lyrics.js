module.exports = {
    active: true,
    name: 'lyrics',
    description: 'Seaches a song by its lyrics',
    category: "fun",
    execute() {
        const { client, message, args, db, embed } = require("../app")

        message.channel.startTyping()
        const fetch = require("node-fetch")
        fetch("https://api.ksoft.si/lyrics/search?limit=1&q=" + args.join(" "), {
            headers: {
                "Authorization": "Bearer " + client.cfg.ksoft,
                "User-Agent": "Daydream Bot <" + client.cfg.contact + "> | node-fetch " + (require("../node_modules/node-fetch/package.json").version)
            }
        }).then(response => {
            response.json().then(body => {
                const data = body.data[0]
                const embedLyrics = new embed()
                    .setColor(0x212121)
                    .setFooter("Thanks KSoft.Si!")
                    .setTitle("Found something 🔍")
                    .addField(`Artist: ${data.artist}`, `Album: ${data.album.split(",").join(" / ")}`)
                    .addField(`Song: ${data.name}`, `released ${data.album_year.split(",").join(" / ")}`)
                    .setThumbnail(data.album_art)
                message.channel.send(embedLyrics)
            })
        })
        message.channel.stopTyping()
    },
}