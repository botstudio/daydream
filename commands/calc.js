module.exports = {
    name: "calc",
    description: "Does some calculation for you",
    active: false,
    category: "util",
    execute() {
        const { client, message, args, db, embed } = require("../app")

        const wrongFormat = () => {
            return message.channel.send("<:IconProvide:553870022125027329> Format: `" + client.prefix + "calc num1 [+,-,*,/,%,**,rt] num2`")
        }
        if (!["+", "-", "/", "*", "%", "**", "rt"].includes(args[1]) || !args[2]) return wrongFormat()
        if (isNaN(args[0]) || isNaN(args[2])) return wrongFormat()
        let operation
        let result
        const operator = args[1]
        const num1 = parseFloat(args[0])
        const num2 = parseFloat(args[2])
        const finalNum1 = parseFloat((Math.max(num1, num2)))
        const finalNum2 = parseFloat((Math.min(num1, num2)))

        const make = (result, operation) => {
            const embedCalc = new embed()
                .setColor(0x212121)
                .setTitle(`${num1} ${operation} ${num2}`)
                .setDescription(String(result))
            message.channel.send(embedCalc)
        }

        switch (operator) {
            case "+":
                result = (finalNum1 + finalNum2).toFixed(4)
                operation = "plus"
                make(result, operation)
                break
            case "-":
                result = (num1 - num2).toFixed(4)
                operation = "minus"
                make(result, operation)
                break
            case "*":
                result = (finalNum1 * finalNum2).toFixed(4)
                operation = "by"
                make(result, operation)
                break
            case "/":
                result = (num1 / num2).toFixed(4)
                operation = "divided by"
                make(result, operation)
                break
            case "%":
                result = num1 % num2
                operation = "mod"
                make(result, operation)
                break
            case "**":
                result = Math.pow(num1, num2)
                operation = "exponented by"
                make(result, operation)
                break
            case "rt":
                result = Math.pow(num2, 1 / num1)
                operation = "** 1 /"
                make(result, operation)
                break
            default:
                message.channel.send("Failed with error.")
                break
        }
    },
}