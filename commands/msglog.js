module.exports = {
    name: "msglog",
    description: "Sets up a log channel for deleted messages",
    active: true,
    permissions: [
        "MANAGE_MESSAGES"
    ],
    category: "config",
    execute() {
        const { client, message, args, db } = require("../app")

        db.get("log_"+message.guild.id, (err,val) => {
            if (args[0] === "none") {
                if (val) {
                    message.channel.send("Removed")
                    return db.del("log_" + message.guild.id)
                }
            }
            if (!message.mentions.channels.first()) return message.channel.send("<:IconProvide:553870022125027329> I need a mentioned channel")
    
            db.set("log_" + message.guild.id, message.mentions.channels.first().id)
            message.channel.send("Set! `" + client.prefix + "msglog none` to disable.")
        })
    }
}