module.exports = {
    name: "prefix",
    description: 'Sets the prefix',
    active: true,
    permissions: [
        "MANAGE_MESSAGES"
    ],
    category: "config",
    execute() {
        const { client, message, args, db } = require("../app")

        if (!args[0]) return message.channel.send("<:IconProvide:553870022125027329> I need a prefix to begin with.")
        if (args[0] === "reset") {
            message.channel.send("Prefix reset.")
            return db.del("prefix_" + message.guild.id)
        }
        db.set("prefix_" + message.guild.id, args[0].toLocaleLowerCase(), (err,ok) => {
            if (!ok) throw new Error("Setting prefix not ok: " + err.message)
            db.get("prefix_"+message.guild.id, (err, val) => {
                message.channel.send('Prefix is `' + val + "` now")
            })
        })
        
    },
}