module.exports = {
    name: "vote",
    description: "Vote for me! <3",
    active: true,
    category: "meta",
    execute() {
        const { client, message, args, db, embed } = require("../app")

        const embedVote = new embed()
            .setTitle("Vote for " + client.user.username)
            .setColor(0x212121)
            .setDescription(`[<3](https://discordbots.org/bot/${client.user.id}/vote)`)
        message.channel.send(embedVote)
    },
}