# Contribution Guide  
  
Hello! First of all, thanks for taking your time to read this guide, and hopefully, contributing as a result.  
  
If you found a bug, there are generally a few ways to get it fixed:  
  
### Using `dd-bugreport`  
  
![dd-bugreport Bug Description](https://sp46.s-ul.eu/zrFcclHZ.png)
  
### Opening a Merge Request  
  
If you know how to handle the project stack well, you can write the bugfix yourself. Fork this project to your GitLab account, and clone it to your device afterwards. Fix the bug, and test if your solution works.  
  
After you're sure it works, navigate to the _Merge Requests_ section on the right-hand side of the page. Open a Merge Request with the corresponding data.  
  
### Opening an Issue  
  
If you do not know how to use the project stack well, or not at all, you can open an issue instead. Navigate to the _Issues_ section on your right. Open an issue and fill in a description and preferably some pictures (GitLab hosts these for you, if you want, just drop them in) as well as a title. All of those must be descriptive enough to make the issue reproducible.  
  
### About Security Issues
  
**In no case** should you ever disclose security issues publicly before a project member *explicitly* stated that it is 100% fixed. You can use the vulnerability report tool located at [bs.sec.service.geist.ga](https://bs.sec.service.geist.ga/).  
  
