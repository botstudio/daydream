module.exports = {
    name: "license",
    description: "Get info about a license",
    active: true,
    category: "util",
    execute() {
        const { client, message, args, db, embed } = require("../app")
        const fetch = require("node-fetch")

        if (args[0] === "advice") {
            return message.channel.send("<https://developer.github.com/v3/licenses/> states:\n>>>\
            GitHub is a lot of things, but it’s not a law firm. As such, GitHub does not provide legal advice. Using the Licenses API or sending us an email about it does not constitute legal advice nor does it create an attorney-client relationship. If you have any questions about what you can and can't do with a particular license, you should consult with your own legal counsel before moving forward. In fact, you should always consult with your own lawyer before making any decisions that might have legal ramifications or that may impact your legal rights.\n\n\
            GitHub created the License API to help users get information about open source licenses and the projects that use them. We hope it helps, but please keep in mind that we’re not lawyers (at least not most of us aren't) and that we make mistakes like everyone else. For that reason, GitHub provides the API on an “as-is” basis and makes no warranties regarding any information or licenses provided on or through it, and disclaims liability for damages resulting from using the API.")
        }

        if (!args[0]) return message.channel.send("I need a License.")
        fetch("https://api.github.com/licenses/" + args[0]).then(res => res.json().then(body => {
            if (body.message === "Not Found") return message.channel.send("No such license! Use the SPDX ID.")
            const embedLicense = new embed()
                .setTitle(body.name)
                .setDescription(body.description)
            let permissions = "", conditions = "", limitations = ""
            body.permissions.forEach(p => permissions += "\n" + p)
            body.conditions.forEach(p => conditions += "\n" + p)
            body.limitations.forEach(p => limitations += "\n" + p)
            embedLicense
                .addField("Permissions", (permissions || "<nothing>"), true)
                .addField("Conditions", (conditions || "<nothing>"), true)
                .addField("Limitations", (limitations || "<nothing>"), true)
                .addField("Common?", (body.featured ? "Yes" : "No"))
                .setColor(0x212121)
                .setFooter("Not legal advice - see `license advice`")
            message.channel.send(embedLicense)
        }))
    },
}