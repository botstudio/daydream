module.exports = {
    name: "npm",
    description: "Allows you to view info about an npm package",
    active: true,
    category: "util",
    execute() {
        const { client, message, args, db, embed } = require("../app")

        const fetch = require("node-fetch")
        const query = args.join(" ")
        if (!query) return message.channel.send("<:IconProvide:553870022125027329> I need a package name")
        fetch("https://registry.yarnpkg.com/" + query).then(res => {
            res.json().then(pkg => {
                const embedYarn = new embed()
                    .setColor(0xfb3b49)
                const success = pkg.error ? false : true
                if (!success) {
                    embedYarn
                        .setTitle("<:IconProvide:553870022125027329> Nothing found")
                        .setDescription("Try something different.")
                    return message.channel.send(embedYarn)
                } else {
                    const versionKeys = Object.keys(pkg.versions)
                    // ⬇ get versions object, use some js magic to get newest version and continue with that, yarn registry is weird ⬇
                    const versionInfo = pkg.versions[versionKeys.slice(-1)[0]]
                    embedYarn
                        .setTitle("Result")
                        .addField("Name", `\`${pkg.name}\``, true)
                        .addField("Description", pkg.description, true)
                        .addField("Current Version", versionInfo.version, true)
                        .addField("Keywords", `\`${pkg.keywords||"<nothing>"}\``, true)
                        .addField("Author", `NPM says \`${pkg.maintainers[0].name}\` | package.json says ${versionInfo.author.name}`)
                    if (query.startsWith("@") && query.includes("/")) {
                        console.log(pkg.name)
                        console.log(typeof pkg.name)
                        embedYarn.addField("Scope", `\`${pkg.name.split(/\//)[0].substr(1)}\``, true)
                    }
                    return message.channel.send(embedYarn)
                }
            })
        })
    }
}