module.exports = {
    name: "banfrombugs",
    description: "Ban a user from bug reporting.",
    category: "meta",
    active: true,
    admin: true,
    execute() {
        const { client, message, args, db } = require("../app")

        if (args[0] === "add") {
            if (!args[1]) return message.channel.send("<:IconProvide:553870022125027329> Provide the user ID of the misbehaving user.")
            db.sadd("bugs:banned", args[1])
            message.channel.send("Done.")
        } else if (args[0] === "remove") {
            if (!args[1]) return message.channel.send("<:IconProvide:553870022125027329> Provide the user ID of the user to get unbanned.")
            db.srem("bugs:banned", args[1])
            message.channel.send("Done.")
        } else {
            return message.channel.send(" <:IconThis:553869005820002324> Format: `" + client.prefix + "banfrombugs [add,remove] userID`")
        }
    },
}