module.exports = {
    name: "pull",
    description: "Pull users into your voice channel",
    category: "util",
    permissions: [
        "MOVE_MEMBERS"
    ],
    active: true,
    execute() {
        const { client, message, args, db, embed } = require("../app")

        if (!message.member.voiceChannel) return message.channel.send("You're not in a voice channel")
        if (!message.mentions.members.first()) return message.channel.send("<:IconProvide:553870022125027329> I need users do to that.")

        message.channel.send("Please wait...").then(m => {
            message.mentions.members.forEach(member => {
                if (!member.voiceChannel) {
                    m.edit(`${m.content}\n${member.user.tag} is not in a voice channel - skipping!`)
                } else {
                    member.setVoiceChannel(message.member.voiceChannel).then(() => {
                        m.edit(`${m.content}\n${member.user.tag} was moved successfully`)
                    })
                }
                
            })
        })
    },
}