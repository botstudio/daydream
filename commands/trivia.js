module.exports = {
    active: true,
    name: 'trivia',
    description: 'Plays a game of trivia',
    category: "fun",
    execute() {
        const { client, message, args, db, embed } = require("../app")

        const fetch = require("node-fetch")
        const he = require("he")
        fetch("https://opentdb.com/api.php?amount=1").then(res => res.json().then(body => {
            const answers = body.results[0].incorrect_answers.concat(body.results[0].correct_answer).sort()
            const filter = (response) => {
                return body.results[0].correct_answer.toLowerCase() === response.content.toLowerCase()
            }
            const embedTrivia = new embed()
                .setTitle("Trivia")
                .setColor(0x212121)
                .addField(he.decode(body.results[0].category), he.decode(body.results[0].question))
                .addField("Choices:", he.decode(answers.join(", ")))
                .setFooter("Type your answer in this channel")
            message.channel.send(embedTrivia).then(() => {
                message.channel.awaitMessages(filter, { maxMatches: 1, time: 30000, errors: ["time"] })
                    .then(collected => {
                        message.channel.send(`${collected.first().author} got it!`)
                    })
                    .catch(collected => {
                        message.channel.send("Nobody got the answer this time. Try again, maybe?")
                    })
            })
        }))
    },
}