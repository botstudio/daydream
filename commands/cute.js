module.exports = {
    name: "cute",
    description: "Shows you a cute picture... Aww :3",
    active: true,
    category: "image",
    execute() {
        const { client, message, args, db, embed } = require("../app")
        
        message.channel.startTyping()
        const fetch = require("node-fetch")
        fetch("https://api.ksoft.si/images/random-image?tag=fox", {
            headers: {
                "Authorization": "Bearer " + client.cfg.ksoft,
                "User-Agent": "Daydream Bot <" + client.cfg.contact + "> | node-fetch " + (require("../node_modules/node-fetch/package.json").version)
            }
        }).then(response => {
            if (!response.ok) return message.channel.send("Errored while trying to connect to server.")
            response.json().then(body =>{
                const embedCute = new embed()
                    .setColor(0x212121)
                    .setImage(body.url)
                    .setTitle("Here's a cute fox :3")
                    .setFooter("Served from an external source (KSoft.Si), so please report NSFW with the `bugreport` command.")
                message.channel.send(embedCute)
            })
        })
        message.channel.stopTyping()
    },
}