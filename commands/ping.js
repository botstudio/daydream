module.exports = {
	active: true,
	name: 'ping',
	description: 'Pings!',
	category: "meta",
	async execute() {
		const { client, message, args, db } = require("../app")

		const m = await message.channel.send("⏱")
		m.edit(`<:IconThis:553869005820002324> Latency: ${m.createdTimestamp - message.createdTimestamp}ms. API Latency is ${Math.round(client.ping)}ms`)
	},
}