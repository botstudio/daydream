module.exports = {
    name: "yn",
    description: "Answer a yes/no question",
    active: true,
    category: "image",
    execute() {
        const { client, message, args, db, embed } = require("../app")
        const fetch = require("node-fetch")

        const capFirst = (string) => {
            if (typeof string === undefined) return
            const firstLetter = string[0] || string.charAt(0)
            return firstLetter ? firstLetter.toUpperCase() + string.substr(1) : ''
        }

        fetch("https://yesno.wtf/api").then(res => res.json()).then(body => {
            const embedYn = new embed()
                .setColor(0x212121)
                .setTitle(capFirst(body.answer) + "!")
                .setImage(body.image)
            message.channel.send(embedYn)
        })
    },
}